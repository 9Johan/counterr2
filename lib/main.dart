import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() => runApp(const ProviderScope(child: MyApp()));

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: MainPage());
  }
}

final counterProvider = StateProvider<int>((ref) => 0);

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text(
            'Counter',
            style: TextStyle(fontSize: 20, color: Colors.black87),
          ),
          centerTitle: true),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Consumer(builder: (context, ref, _) {
            final counterState = context.read(counterProvider).state;
            return Text('$counterState', style: const TextStyle(fontSize: 50));
          }),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                  onPressed: () {
                    context.read(counterProvider);
                  },
                  icon: const Icon(Icons.plus_one)),
              IconButton(
                  onPressed: () {
                    context.read(counterProvider);
                  },
                  icon: const Icon(Icons.exposure_minus_1)),
            ],
          ),
        ],
      ),
    );
  }
}
